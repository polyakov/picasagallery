import os
import sys
# hack for utf8 long_description support
reload(sys).setdefaultencoding("UTF-8")
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

version='1.0.0'

setup(
    name = 'picasagallery',
    version = version,
    author = 'Pavel Polyakov',
    author_email = 'pollydrag@yandex.ru',
    url = 'http://bitbucket.org/ppolyakov/picasagallery',
    download_url = 'http://bitbucket.org/ppolyakov/picasagallery/get/tip.zip',

    description = 'Django gallery app based on Google Picasa',
    long_description = open('README.rst').read().decode('utf8'),
    license = 'MIT license',
    requires = ['django (>=1.3)','django_sekizai','gdata',],

    packages=find_packages(),
    package_data={'picasagallery': ['templates/picasagallery/*', 
                                    'static/picasagallery/css/*', 
                                    'static/prettyphoto/css/*',
                                    'static/prettyphoto/images/fullscreen/*',
                                    'static/prettyphoto/images/thumbnails/*',
                                    'static/prettyphoto/images/prettyPhoto/dark_rounded/*',
                                    'static/prettyphoto/images/prettyPhoto/dark_square/*',
                                    'static/prettyphoto/images/prettyPhoto/default/*',
                                    'static/prettyphoto/images/prettyPhoto/facebook/*',
                                    'static/prettyphoto/images/prettyPhoto/light_rounded/*',
                                    'static/prettyphoto/images/prettyPhoto/light_square/*',
                                    'static/prettyphoto/js/*',
                                    
                                    ]},
    

    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Natural Language :: Russian',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    include_package_data=True,
    zip_safe = False,
    install_requires=['django>=1.3','django_sekizai','gdata'],
)
